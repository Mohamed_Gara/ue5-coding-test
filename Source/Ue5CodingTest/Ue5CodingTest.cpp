// Copyright (C) 2022 Humanitas Solutions. All rights reserved.


#include "Ue5CodingTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE(FDefaultGameModuleImpl, Ue5CodingTest, "Ue5CodingTest");
