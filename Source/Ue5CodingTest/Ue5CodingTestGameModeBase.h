// Copyright (C) 2022 Humanitas Solutions. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Ue5CodingTestGameModeBase.generated.h"

UCLASS()
class UE5CODINGTEST_API AUe5CodingTestGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
};
