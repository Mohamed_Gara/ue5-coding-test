// Copyright (C) 2022 Humanitas Solutions. All rights reserved.

using UnrealBuildTool;

public class Ue5CodingTestTarget : TargetRules
{
	public Ue5CodingTestTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("Ue5CodingTest");
	}
}
