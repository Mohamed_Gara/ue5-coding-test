// Copyright (C) 2022 Humanitas Solutions. All rights reserved.

using UnrealBuildTool;

public class Ue5CodingTestEditorTarget : TargetRules
{
	public Ue5CodingTestEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;
		ExtraModuleNames.Add("Ue5CodingTest");
	}
}
